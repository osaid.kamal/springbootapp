package com.sts.dao;

import org.springframework.data.repository.CrudRepository;

import com.sts.entities.User;

public interface UserRepo extends CrudRepository<User, Integer> {

}