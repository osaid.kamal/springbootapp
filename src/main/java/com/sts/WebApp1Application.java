package com.sts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.sts.dao.UserRepo;
import com.sts.entities.User;

@SpringBootApplication
public class WebApp1Application {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(WebApp1Application.class, args);
		UserRepo urp = context.getBean(UserRepo.class);
		User user = new User();
		user.setName("Osaid");
		user.setCity("Delhi");
		user.setStatus("I am Java");
		User user1 = urp.save(user);
		System.out.println(user1);

	}
}
